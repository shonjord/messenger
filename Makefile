NO_COLOR        =\033[0m
OK_COLOR        =\033[32;01m
ERROR_COLOR     =\033[31;01m
WARN_COLOR      =\033[33;01m
DB_CONTAINER    = postgres
DB_NAME         = messenger
DB_USER         = postgres
DB_DUMP         = db/dumps/messenger.sql
DB_CONTAINER_ID = $(shell docker ps -aqf "name=$(DB_CONTAINER)")
APP_SYNC        = $(shell docker ps -aqf "name=app-sync")

install:
	@if grep -q messenger /etc/hosts;\
	then echo "$(ERROR_COLOR)==> Application is already installed, run make start instead $(NO_COLOR)";\
	else make install_app;\
	fi;

install_app:
	@echo "$(WARN_COLOR)==> Installing the Application $(WARN_COLOR)"
	@echo "$(OK_COLOR)==> Registering new entry in the hosts $(NO_COLOR)"
	@echo "" | sudo tee -a /etc/hosts
	@echo "# Messenger Application" | sudo tee -a /etc/hosts
	@echo "127.0.0.1 messenger.dev" | sudo tee -a /etc/hosts
	@make start

start:
	@echo "$(OK_COLOR)==> Initializing the Application $(NO_COLOR)"
	@echo "$(OK_COLOR)==> Creating App-Sync volume $(NO_COLOR)"
	@docker volume create --name=app-sync
	@echo "$(OK_COLOR)==> Initializing containers $(NO_COLOR)"
	@docker-compose up -d
	@echo "$(OK_COLOR)==> Starting App-Sync $(NO_COLOR)"
	@docker-sync start
	@echo "$(OK_COLOR)==> Installing dependencies if there are any $(NO_COLOR)"
	@docker-compose exec app composer install
	@echo "$(OK_COLOR)==> Importing DB into the container $(NO_COLOR)"
	@make import_db

import_db:
	@docker-compose exec $(DB_CONTAINER) psql -U $(DB_USER) -c "DROP DATABASE $(DB_NAME)"
	@docker-compose exec $(DB_CONTAINER) psql -U $(DB_USER) -c "CREATE DATABASE $(DB_NAME)"
	@echo $(DB_CONTAINER_ID)
	@cat $(DB_DUMP) | docker exec -i $(DB_CONTAINER_ID) psql -U $(DB_USER) $(DB_NAME)

stop:
	@echo "$(OK_COLOR)==> Shutting down the Application $(NO_COLOR)"
	@docker-compose down
	@docker-sync stop
	@docker rm $(APP_SYNC)
