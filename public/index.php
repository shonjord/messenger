<?php declare(strict_types = 1);

/* @author Albert Agelviz <aagelviz@gmail.com>
|--------------------------------------------------------------------------
| Autoload requirement
|--------------------------------------------------------------------------
|
| This step will autoload the vendor folder, and require all the dependencies
| of this application.
|
 */
require_once __DIR__ . '/../vendor/autoload.php';

/*
|--------------------------------------------------------------------------
| Instantiate the Application
|--------------------------------------------------------------------------
|
| This step will instantiate the Application
*/
$app = new \Messenger\Application\Kernel\Messenger;

/*
|--------------------------------------------------------------------------
| Run the Application
|--------------------------------------------------------------------------
|
| Once all of the dependencies of the application are set
| the application is ready to run.
*/
$app->run();
