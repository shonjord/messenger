<?php declare(strict_types=1);

namespace Messenger\Infrastructure\CommandBus\Bus;

use Collections\Map;
use League\Tactician\Handler\Locator\InMemoryLocator;
use League\Tactician\Handler\CommandHandlerMiddleware;
use League\Tactician\CommandBus as TacticianCommandBus;
use Messenger\Infrastructure\CommandBus\Command\CommandInterface;
use Messenger\Infrastructure\CommandBus\Handler\HandlerInterface;
use League\Tactician\Handler\MethodNameInflector\HandleInflector;
use League\Tactician\Handler\CommandNameExtractor\ClassNameExtractor;
use Messenger\Infrastructure\CommandBus\Exception\CommandHandlersCantBeEmptyException;

final class CommandBus implements CommandBusInterface
{
    /**
     * @var Map
     */
    private $handlers;

    /**
     * @var HandleInflector
     */
    private $inflector;

    /**
     * @var InMemoryLocator
     */
    private $locator;

    /**
     * @var ClassNameExtractor
     */
    private $extractor;

    /**
     * @param array $handlers
     * @throws CommandHandlersCantBeEmptyException
     */
    public function __construct(array $handlers)
    {
        if (empty($handlers)) {
            throw new CommandHandlersCantBeEmptyException;
        }
        $this->handlers  = new Map($handlers);
        $this->inflector = new HandleInflector;
        $this->locator   = new InMemoryLocator;
        $this->extractor = new ClassNameExtractor;
    }

    /**
     * @param CommandInterface $command
     */
    public function handle(CommandInterface $command) : void
    {
        $this->handlers->each(function (HandlerInterface $handler, string $command) : void {
            $this->locator->addHandler($handler, $command);
        });
        $this->getCommandBus()->handle($command);
    }

    /**
     * @return TacticianCommandBus
     */
    private function getCommandBus() : TacticianCommandBus
    {
        return new TacticianCommandBus($this->getMiddleWares());
    }

    /**
     * @return array
     */
    private function getMiddleWares() : array
    {
        return [
            new CommandHandlerMiddleware($this->extractor, $this->locator, $this->inflector)
        ];
    }
}
