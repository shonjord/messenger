<?php declare(strict_types=1);

namespace Messenger\Infrastructure\CommandBus\Bus;

use Messenger\Infrastructure\CommandBus\Command\CommandInterface;

interface CommandBusInterface
{
    /**
     * @param CommandInterface $command
     */
    public function handle(CommandInterface $command) : void;
}
