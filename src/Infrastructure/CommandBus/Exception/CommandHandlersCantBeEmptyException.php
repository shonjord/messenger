<?php declare(strict_types=1);

namespace Messenger\Infrastructure\CommandBus\Exception;

use Messenger\Application\Exception\MessengerException;

final class CommandHandlersCantBeEmptyException extends MessengerException
{
    /**
     * @var string
     */
    public $message = 'Command handlers can\'t be empty';
}
