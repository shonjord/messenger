<?php declare(strict_types=1);

namespace Messenger\Infrastructure\CommandBus\Handler;

use Messenger\Infrastructure\CommandBus\Command\CommandInterface;

interface HandlerInterface
{
    /**
     * @param CommandInterface $command
     * @return void
     */
    public function handle(CommandInterface $command) : void;
}
