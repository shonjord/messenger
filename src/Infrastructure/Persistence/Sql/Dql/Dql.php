<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Persistence\Sql\Dql;

use Collections\Map;
use Collections\Pair;

final class Dql
{
    public const ALL = '*';

    /**
     * @var array
     */
    private $sql;

    /**
     * @var string
     */
    private $table;

    /**
     * @param string $column
     * @return Dql
     */
    public function select(string $column) : Dql
    {
        $this->sql['statement']['select'] = "SELECT ${column}";

        return $this;
    }

    /**
     * @param string $table
     * @return Dql
     */
    public function from(string $table) : Dql
    {
        $this->setTable($table);
        $this->sql['statement']['from'] = "FROM {$table}";

        return $this;
    }

    /**
     * @param string $condition
     * @return Dql
     */
    public function where(string $condition) : Dql
    {
        $this->sql['statement']['where'] = "WHERE {$condition}";

        return $this;
    }

    /**
     * @param string $condition
     * @return Dql
     */
    public function and(string $condition) : Dql
    {
        $this->sql['statement']['and'][] = "AND ${condition}";

        return $this;
    }

    /**
     * @param string $table
     * @return $this
     */
    public function insertInto(string $table)
    {
        $this->setTable($table);
        $this->sql['statement']['insert'] = "INSERT INTO {$table}";

        return $this;
    }

    /**
     * @param array $values
     * @return $this
     */
    public function values(array $values)
    {
        $data            = $this->prepareData($values);
        $columns         = $this->createColumnsFrom($data);
        $inputParameters = $this->createInputParametersFrom($columns, $data);
        $values          = $this->createValuesFrom($data);

        $this->sql['input_parameters']    = $inputParameters;
        $this->sql['statement']['values'] = "({$columns}) VALUES ({$values})";

        return $this;
    }

    /**
     * @return string
     */
    public function getStatement() : string
    {
        return implode(' ', $this->sql['statement']);
    }

    /**
     * @return array
     */
    public function getInputParameters() : array
    {
        return $this->sql['input_parameters'];
    }

    /**
     * @return string|null
     */
    public function getTable() : ?string
    {
        return $this->table;
    }

    /**
     * @param string $table
     * @return Dql
     */
    private function setTable(string $table) : Dql
    {
        $this->table = $table;

        return $this;
    }

    /**
     * @param array $data
     * @return string
     */
    private function createColumnsFrom(array $data) : string
    {
        return implode(', ', array_map(function (string $value) : string {
            return str_replace(':', '', $value);
        }, array_keys($data)));
    }

    /**
     * @param string $columns
     * @param array $data
     * @return array
     */
    private function createInputParametersFrom(string $columns, array $data) : array
    {
        return array_combine(explode(', ', $columns), array_values($data));
    }

    /**
     * @param array $data
     * @return string
     */
    private function createValuesFrom(array $data) : string
    {
        return implode(', ', array_keys($data));
    }

    /**
     * @param array $data
     * @return array
     */
    private function prepareData(array $data) : array
    {
        $values = new Map;

        array_walk($data, function ($value, string $key) use ($values) : void {
            $values->add(new Pair(":{$key}", $value));
        });
        return $values->toArray();
    }
}
