<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Persistence\Sql\Connection;

use Messenger\Domain\Common\Entity\EntityInterface;
use Ramsey\Uuid\UuidInterface;

interface ConnectionInterface
{
    /**
     * @param EntityInterface $entity
     * @return void
     */
    public function persist(EntityInterface $entity) : void;

    /**
     * @param EntityInterface $entity
     */
    public function persistCaching(EntityInterface $entity) : void;

    /**
     * @param string $table
     * @param UuidInterface $uuid
     * @return EntityInterface|null
     */
    public function findById(string $table, UuidInterface $uuid) : ?EntityInterface;

    /**
     * @param string $table
     * @return array
     */
    public function findAll(string $table) : array;
}
