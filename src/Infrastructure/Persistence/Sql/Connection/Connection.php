<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Persistence\Sql\Connection;

use Ramsey\Uuid\UuidInterface;
use Messenger\Domain\Common\Entity\EntityInterface;
use Messenger\Infrastructure\Persistence\Sql\Dql\Dql;
use Messenger\Infrastructure\Serializer\Common\SerializerInterface;
use Messenger\Infrastructure\Persistence\Redis\Connection\ConnectionInterface as RedisInterface;

final class Connection implements ConnectionInterface
{
    private const KEY_PATTERN   = '*%s*';
    private const KEY_SIGNATURE = '%s-%s';

    /**
     * @var \PDO
     */
    private $pdo;

    /**
     * @var RedisInterface
     */
    private $redis;

    /**
     * @var Dql
     */
    private $dql;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param \PDO $pdo
     * @param RedisInterface $redis
     * @param SerializerInterface $serializer
     */
    public function __construct(\PDO $pdo, RedisInterface $redis, SerializerInterface $serializer)
    {
        $this->pdo        = $pdo;
        $this->redis      = $redis;
        $this->serializer = $serializer;
    }

    /**
     * @param EntityInterface $entity
     * @return void
     */
    public function persist(EntityInterface $entity) : void
    {
        $this->createQuery()
             ->insertInto($entity->getName())
             ->withValues($this->serializer->serialize($entity))
             ->prepareAndExecute();
    }

    /**
     * @param EntityInterface $entity
     */
    public function persistCaching(EntityInterface $entity): void
    {
        $this->persist($entity);
        $this->cache($entity);
    }

    /**
     * @param string $table
     * @return array
     */
    public function findAll(string $table) : array
    {
        return $this->createQuery()
                    ->select(Dql::ALL)
                    ->from($table)
                    ->deserializeCollection();
    }

    /**
     * @param string $table
     * @param UuidInterface $uuid
     * @return EntityInterface|null
     */
    public function findById(string $table, UuidInterface $uuid): ?EntityInterface
    {
        $key = sprintf(self::KEY_SIGNATURE, $table, $uuid);

        if ($this->redis->exists($key)) {
            return $this->deserialize([$this->redis->find($key)], $table)[0];
        }

        $data = $this->findByIdUsingSql($table, $uuid);
        return empty($data) ? null : $this->deserialize($data, $table)[0];
    }

    /**
     * @param string $table
     * @param UuidInterface $uuid
     * @return array
     */
    private function findByIdUsingSql(string $table, UuidInterface $uuid) : array
    {
        return $this->createQuery()
                    ->select(Dql::ALL)
                    ->from($table)
                    ->where(sprintf('id = \'%s\'', $uuid))
                    ->queryAndFetch();
    }

    /**
     * @return array
     */
    private function deserializeCollection() : array
    {
        $data = $this->redis->findAll(sprintf(self::KEY_PATTERN, $this->dql->getTable()));
        if (empty($data)) {
            $data = $this->queryAndFetch();
        }
        return $this->deserialize($data, $this->dql->getTable());
    }

    /**
     * @param $data
     * @param string $table
     * @return array
     */
    private function deserialize($data, string $table) : array
    {
        return $this->serializer->deserialize($data, $table);
    }

    /**
     * @return Connection
     */
    private function createQuery(): Connection
    {
        $this->dql = new Dql;

        return $this;
    }

    /**
     * @param array $values
     * @return Connection
     */
    private function withValues(array $values): Connection
    {
        $this->dql->values($values);

        return $this;
    }

    /**
     * @param string $column
     * @return Connection
     */
    private function select(string $column) : Connection
    {
        $this->dql->select($column);

        return $this;
    }

    /**
     * @param string $table
     * @return Connection
     */
    private function from(string $table) : Connection
    {
        $this->dql->from($table);

        return $this;
    }

    /**
     * @param string $conditions
     * @return Connection
     */
    private function where(string $conditions) : Connection
    {
        $this->dql->where($conditions);

        return $this;
    }

    /**
     * @param string $table
     * @return Connection
     */
    private function insertInto(string $table): Connection
    {
        $this->dql->insertInto($table);

        return $this;
    }

    /**
     * @return array
     */
    private function queryAndFetch() : array
    {
        return $this->pdo->query($this->dql->getStatement())->fetchAll();
    }

    /**
     * @return void
     */
    private function prepareAndExecute() : void
    {
        $this->pdo->prepare($this->dql->getStatement())
                  ->execute($this->dql->getInputParameters());
    }

    /**
     * @param EntityInterface $entity
     * @return void
     */
    private function cache(EntityInterface $entity) : void
    {
        $this->redis->save(
            sprintf(self::KEY_SIGNATURE, $entity->getName(), $entity->getId()),
            json_encode($this->dql->getInputParameters())
        );
    }
}
