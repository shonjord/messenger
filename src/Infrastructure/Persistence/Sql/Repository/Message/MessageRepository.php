<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Persistence\Sql\Repository\Message;

use Ramsey\Uuid\UuidInterface;
use Messenger\Domain\Message\Entity\Message;
use Messenger\Domain\Common\Entity\EntityInterface;
use Messenger\Domain\Message\Collection\MessageCollection;
use Messenger\Domain\Message\Repository\MessageRepositoryInterface;
use Messenger\Infrastructure\Persistence\Sql\Connection\ConnectionInterface;

final class MessageRepository implements MessageRepositoryInterface
{
    /**
     * @var ConnectionInterface
     */
    private $connection;

    /**
     * @param ConnectionInterface $connection
     */
    public function __construct(ConnectionInterface $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param Message $message
     * @return void
     */
    public function save(Message $message) : void
    {
        $this->connection->persistCaching($message);
    }

    /**
     * @param UuidInterface $uuid
     * @return Message|EntityInterface|null
     */
    public function findById(UuidInterface $uuid): ?Message
    {
        return $this->connection->findById(Message::NAME, $uuid);
    }

    /**
     * @return MessageCollection
     */
    public function findAll() : MessageCollection
    {
        return new MessageCollection($this->connection->findAll(Message::NAME));
    }
}
