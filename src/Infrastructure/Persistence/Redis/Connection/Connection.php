<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Persistence\Redis\Connection;

use Predis\Client;

final class Connection implements ConnectionInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @param string $key
     * @param string $value
     * @return void
     */
    public function save(string $key, string $value) : void
    {
        if ($this->isNotAlive()) {
            return;
        }
        $this->client->set($key, $value);
    }

    /**
     * @param string $pattern
     * @return array
     */
    public function findAll(string $pattern) : array
    {
        if ($this->isNotAlive()) {
            return [];
        }
        return array_map(function (string $key) : array {
            return $this->find($key);
        }, $this->client->keys($pattern));
    }

    /**
     * @param string $id
     * @return array
     */
    public function find(string $id) : array
    {
        if ($this->isNotAlive()) {
            return [];
        }
        return $this->exists($id) ? json_decode($this->client->get($id), true) : [];
    }

    /**
     * @param string $key
     * @return bool
     */
    public function exists(string $key) : bool
    {
        if ($this->isNotAlive()) {
            return false;
        }
        return 1 === $this->client->exists($key);
    }

    /**
     * @return bool
     */
    private function isNotAlive() : bool
    {
        try {
            $this->client->ping();
            return false;
        } catch (\Throwable $exception) {
            return true;
        }
    }
}
