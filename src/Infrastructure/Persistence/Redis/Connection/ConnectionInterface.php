<?php

namespace Messenger\Infrastructure\Persistence\Redis\Connection;

interface ConnectionInterface
{
    /**
     * @param string $key
     * @param string $value
     * @return void
     */
    public function save(string $key, string $value) : void;

    /**
     * @param string $id
     * @return array
     */
    public function find(string $id) : array;

    /**
     * @param string $pattern
     * @return array
     */
    public function findAll(string $pattern) : array;

    /**
     * @param string $key
     * @return bool
     */
    public function exists(string $key) : bool;
}
