<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Serializer\Jms\Handlers;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;
use Messenger\Domain\Message\ValueObject\Text;
use JMS\Serializer\Handler\SubscribingHandlerInterface;

final class TextHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods() : array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'Text',
                'method'    => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format'    => 'json',
                'type'      => 'Text',
                'method'    => 'deserialize'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param Text $text
     * @param array $type
     * @param Context $context
     * @return string
     */
    public function serialize(VisitorInterface $visitor, Text $text, array $type, Context $context) : string
    {
        return $visitor->visitString((string) $text, $type, $context);
    }

    /**
     * @param VisitorInterface $visitor
     * @param string $value
     * @param array $type
     * @param Context $context
     * @return Text
     */
    public function deserialize(VisitorInterface $visitor, string $value, array $type, Context $context) : Text
    {
        return new Text($visitor->visitString($value, $type, $context));
    }
}
