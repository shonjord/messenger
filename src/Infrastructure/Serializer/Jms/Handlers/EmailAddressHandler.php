<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Serializer\Jms\Handlers;

use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\VisitorInterface;
use Messenger\Domain\Message\ValueObject\EmailAddress;
use JMS\Serializer\Handler\SubscribingHandlerInterface;

final class EmailAddressHandler implements SubscribingHandlerInterface
{
    /**
     * @return array
     */
    public static function getSubscribingMethods() : array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'EmailAddress',
                'method'    => 'serialize'
            ],
            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format'    => 'json',
                'type'      => 'EmailAddress',
                'method'    => 'deserialize'
            ]
        ];
    }

    /**
     * @param VisitorInterface $visitor
     * @param EmailAddress $email
     * @param array $type
     * @param Context $context
     * @return string
     */
    public function serialize(VisitorInterface $visitor, EmailAddress $email, array $type, Context $context) : string
    {
        return $visitor->visitString((string) $email, $type, $context);
    }

    /**
     * @param VisitorInterface $visitor
     * @param string $value
     * @param array $type
     * @param Context $context
     * @return EmailAddress
     */
    public function deserialize(VisitorInterface $visitor, string $value, array $type, Context $context) : EmailAddress
    {
        return new EmailAddress($visitor->visitString($value, $type, $context));
    }
}
