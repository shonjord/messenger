<?php declare(strict_types = 1);

namespace Messenger\Infrastructure\Serializer\Jms;

use Collections\MapInterface;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\Handler\HandlerRegistry;
use JMS\Serializer\Serializer as SerializerLibrary;
use Messenger\Infrastructure\Serializer\Jms\Handlers\TextHandler;
use Messenger\Infrastructure\Serializer\Common\SerializerInterface;
use Messenger\Infrastructure\Serializer\Jms\Handlers\DateTimeHandler;
use Messenger\Infrastructure\Serializer\Jms\Handlers\EmailAddressHandler;

/**
 * Serialize|Deserialize data using JMS library
 * @author Albert Agelviz <aagelviz@gmail.com>
 */
final class Serializer implements SerializerInterface
{
    private const METADATA_DIR = '/var/www/html/src/Application/Resources/Serializer';

    /**
     * @var SerializerLibrary
     */
    private $serializer;

    public function __construct()
    {
        $this->serializer = $this->getSerializer();
    }

    /**
     * @param string $data
     * @param $entity
     * @return array|mixed|object
     */
    public function deserialize($data, $entity)
    {
        return $this->serializer->deserialize($this->serialize($data), $entity, 'json');
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function serialize($data)
    {
        return $this->serializer->serialize($data instanceof MapInterface ? $data->toArray() : $data, 'json');
    }

    /**
     * @return SerializerLibrary
     */
    private function getSerializer() : SerializerLibrary
    {
        return SerializerBuilder::create()->addMetadataDir(self::METADATA_DIR)
                                          ->configureHandlers($this->getHandlers())
                                          ->build();
    }

    /**
     * @return \Closure
     */
    private function getHandlers() : \Closure
    {
        return function (HandlerRegistry $registry) : void {
            $registry->registerSubscribingHandler(new EmailAddressHandler);
            $registry->registerSubscribingHandler(new TextHandler);
            $registry->registerSubscribingHandler(new DateTimeHandler);
        };
    }
}
