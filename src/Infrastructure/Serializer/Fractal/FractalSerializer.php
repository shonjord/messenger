<?php declare(strict_types=1);

namespace Messenger\Infrastructure\Serializer\Fractal;

use Collections\Enumerable;
use League\Fractal\Manager;
use League\Fractal\Resource\Item;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\ResourceInterface;
use Messenger\Infrastructure\Serializer\Common\SerializerInterface;

final class FractalSerializer implements SerializerInterface
{
    /**
     * @var Manager
     */
    private $manager;

    /**
     * @var FractalResources
     */
    private $resources;

    public function __construct()
    {
        $this->manager   = new Manager;
        $this->resources = new FractalResources;
    }

    /**
     * @param $data
     * @param $entity
     * @return array
     */
    public function deserialize($data, $entity)
    {
        return $this->createDataFrom(
            new Item($data, $this->resources->at($entity))
        );
    }

    /**
     * @param $data
     * @return mixed|string
     */
    public function serialize($data)
    {
        return $this->createDataFrom(
            $data instanceof Enumerable ? $this->getCollectionFrom($data) : $this->getItemFrom($data)
        );
    }

    /**
     * @param ResourceInterface $resource
     * @return array
     */
    private function createDataFrom(ResourceInterface $resource) : array
    {
        return $this->manager->createData($resource)->toArray()['data'];
    }

    /**
     * @param Enumerable $enumerable
     * @return Collection
     */
    private function getCollectionFrom(Enumerable $enumerable) : Collection
    {
        return new Collection($enumerable, $this->at($enumerable));
    }

    /**
     * @param $data
     * @return Item
     */
    private function getItemFrom($data) : Item
    {
        return new Item($data, $this->at($data));
    }

    /**
     * @param $data
     * @return TransformerInterface|null
     */
    private function at($data) : ?TransformerInterface
    {
        return $this->resources->at($data);
    }
}
