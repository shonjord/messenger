<?php

namespace Messenger\Infrastructure\Serializer\Fractal;

use Collections\Map;
use Messenger\Domain\Message\Entity\Message;
use Messenger\Domain\Common\Entity\EntityInterface;
use Messenger\Domain\Message\Collection\MessageCollection;
use Messenger\Application\Resources\Fractal\Message\SerializeMessage;
use Messenger\Application\Resources\Fractal\Message\DeserializeMessage;

final class FractalResources
{
    /**
     * @var Map
     */
    private $resources;

    public function __construct()
    {
        $this->resources = $this->getResources();
    }

    /**
     * @param string|EntityInterface $entity
     * @return TransformerInterface|null
     */
    public function at($entity) : ?TransformerInterface
    {
        return $this->resources->at(is_string($entity) ? $entity : $this->getNameFrom($entity));
    }

    /**
     * @return Map
     */
    private function getResources() : Map
    {
        return new Map([
            Message::class           => new SerializeMessage,
            MessageCollection::class => new SerializeMessage,
            Message::NAME            => new DeserializeMessage
        ]);
    }

    /**
     * @param $entity
     * @return string
     */
    private function getNameFrom($entity) : string
    {
        return (new \ReflectionClass($entity))->getName();
    }
}
