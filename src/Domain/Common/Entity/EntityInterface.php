<?php

namespace Messenger\Domain\Common\Entity;

use Ramsey\Uuid\UuidInterface;

interface EntityInterface
{
    /**
     * @return string
     */
    public function getName() : string;

    /**
     * @return UuidInterface
     */
    public function getId() : UuidInterface;
}
