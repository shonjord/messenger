<?php declare(strict_types=1);

namespace Messenger\Domain\Message\Entity;

use Ramsey\Uuid\UuidInterface;
use Messenger\Domain\Message\ValueObject\Text;
use Messenger\Domain\Common\Entity\EntityInterface;
use Messenger\Domain\Message\ValueObject\EmailAddress;

final class Message implements EntityInterface
{
    public const NAME = 'message';

    /**
     * @var UuidInterface
     */
    private $id;

    /**
     * @var Text
     */
    private $text;

    /**
     * @var null|EmailAddress
     */
    private $emailAddress;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @param UuidInterface $id
     * @param Text $text
     * @param null|EmailAddress $emailAddress$
     * @param \DateTime $createdAt
     */
    public function __construct(UuidInterface $id, Text $text, ?EmailAddress $emailAddress, \DateTime $createdAt)
    {
        $this->id           = $id;
        $this->text         = $text;
        $this->emailAddress = $emailAddress;
        $this->createdAt    = $createdAt;
    }

    /**
     * @return UuidInterface
     */
    public function getId() : UuidInterface
    {
        return $this->id;
    }

    /**
     * @return Text
     */
    public function getText() : Text
    {
        return $this->text;
    }

    /**
     * @return null|EmailAddress
     */
    public function getEmail() : ?EmailAddress
    {
        return $this->emailAddress;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt() : \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @return bool
     */
    public function hasEmail() : bool
    {
        return null !== $this->emailAddress;
    }

    /**
     * @return string
     */
    public function getName() : string
    {
        return self::NAME;
    }
}
