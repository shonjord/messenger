<?php declare(strict_types=1);

namespace Messenger\Domain\Message\Handler;

use Ramsey\Uuid\Uuid;
use Messenger\Domain\Message\Entity\Message;
use Messenger\Domain\Message\Command\SaveMessageCommand;
use Messenger\Infrastructure\CommandBus\Handler\HandlerInterface;
use Messenger\Infrastructure\CommandBus\Command\CommandInterface;
use Messenger\Domain\Message\Repository\MessageRepositoryInterface;

final class SaveMessageHandler implements HandlerInterface
{
    /**
     * @var MessageRepositoryInterface
     */
    private $repository;

    /**
     * @param MessageRepositoryInterface $repository
     */
    public function __construct(MessageRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param CommandInterface|SaveMessageCommand $command
     * @return void
     */
    public function handle(CommandInterface $command) : void
    {
        $this->repository->save($this->createMessageFrom($command));
    }

    /**
     * @param CommandInterface|SaveMessageCommand $command
     * @return Message
     */
    private function createMessageFrom(CommandInterface $command) : Message
    {
        return new Message(Uuid::uuid4(), $command->getText(), $command->getEmailAddress(), new \DateTime);
    }
}
