<?php declare(strict_types=1);

namespace Messenger\Domain\Message\ValueObject\Exception;

use Messenger\Application\Exception\AbstractValidationException;

final class InvalidEmailException extends AbstractValidationException
{
    /**
     * @var string
     */
    public $message = 'Invalid e-mail provided';

    /**
     * @param string|null $emailAddress
     */
    public function __construct(string $emailAddress = null)
    {
        if ($emailAddress) {
            $this->message = sprintf('%s : %s', $this->message, $emailAddress);
        }
    }
}
