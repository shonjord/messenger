<?php declare(strict_types=1);

namespace Messenger\Domain\Message\ValueObject\Exception;

final class TextLengthExceedsMaximumException extends \Exception
{
    /**
     * @var string
     */
    public $message = 'Text exceeds maximum length which is 120 characters';
}
