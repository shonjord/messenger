<?php declare(strict_types=1);

namespace Messenger\Domain\Message\ValueObject;

use Messenger\Domain\Message\ValueObject\Exception\InvalidEmailException;

final class EmailAddress
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->getValid($value);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     * @throws \Exception
     */
    private function getValid(string $value) : string
    {
        if ($this->isValid($value)) {
            return $value;
        }
        throw new InvalidEmailException($value);
    }

    /**
     * @param string $value
     * @return bool
     */
    private function isValid(string $value) : bool
    {
        $value  =  preg_replace('/\s+/', '', $value);

        if (is_bool(filter_var(strtolower($value), FILTER_VALIDATE_EMAIL))) {
            return false;
        }
        return true;
    }
}
