<?php declare(strict_types=1);

namespace Messenger\Domain\Message\ValueObject;

use Messenger\Domain\Message\ValueObject\Exception\TextLengthExceedsMaximumException;

final class Text
{
    /**
     * @var string
     */
    private $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $this->getValid($value);
    }

    /**
     * @return string
     */
    public function __toString() : string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return string
     * @throws TextLengthExceedsMaximumException
     */
    private function getValid(string $value) : string
    {
        if (strlen($value) > 120) {
            throw new TextLengthExceedsMaximumException;
        }
        return $value;
    }
}
