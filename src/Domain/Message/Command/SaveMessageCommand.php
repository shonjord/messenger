<?php declare(strict_types=1);

namespace Messenger\Domain\Message\Command;

use Messenger\Domain\Message\ValueObject\Text;
use Messenger\Domain\Message\ValueObject\EmailAddress;
use Messenger\Infrastructure\CommandBus\Command\CommandInterface;

final class SaveMessageCommand implements CommandInterface
{
    /**
     * @var null|string
     */
    private $emailAddress;

    /**
     * @var Text
     */
    private $text;
    
    /**
     * @param null|string $emailAddress
     * @param string $text
     */
    public function __construct(?string $emailAddress, string $text)
    {
        $this->emailAddress = $emailAddress;
        $this->text         = $text;
    }

    /**
     * @return EmailAddress|null
     */
    public function getEmailAddress() : ?EmailAddress
    {
        return null === $this->emailAddress ? null : new EmailAddress($this->emailAddress);
    }

    /**
     * @return Text
     */
    public function getText() : Text
    {
        return new Text($this->text);
    }
}
