<?php declare(strict_types=1);

namespace Messenger\Domain\Message\Repository;

use Messenger\Domain\Message\Entity\Message;
use Messenger\Domain\Message\Collection\MessageCollection;
use Ramsey\Uuid\UuidInterface;

interface MessageRepositoryInterface
{
    /**
     * @param Message $message
     * @return void
     */
    public function save(Message $message) : void;

    /**
     * @param UuidInterface $uuid
     * @return Message|null
     */
    public function findById(UuidInterface $uuid) : ?Message;

    /**
     * @return MessageCollection
     */
    public function findAll() : MessageCollection;
}
