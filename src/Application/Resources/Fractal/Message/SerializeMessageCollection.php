<?php

namespace Intraway\Application\Resources\Fractal\Message;

use Intraway\Domain\Message\Entity\Message;
use League\Fractal\TransformerAbstract;
use Intraway\Domain\Message\Collection\MessageCollection;
use Intraway\Infrastructure\Serializer\Fractal\TransformerInterface;

final class SerializeMessageCollection extends TransformerAbstract implements TransformerInterface
{
    /**
     * @param MessageCollection $collection
     * @return array
     */
    public function transform(MessageCollection $collection) : array
    {
        return ($collection->map(function (Message $message) : array {
            return [
                'id'            => (string) $message->getId(),
                'email_address' => $message->hasEmail() ? (string) $message->getEmail() : null,
                'text'          => (string) $message->getText(),
                'created_at'    => (string) $message->getCreatedAt()->format('Y-m-d H:i:s')
            ];
        }))->toArray();
    }
}
