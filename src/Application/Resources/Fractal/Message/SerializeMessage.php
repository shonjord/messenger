<?php declare(strict_types=1);

namespace Messenger\Application\Resources\Fractal\Message;

use League\Fractal\TransformerAbstract;
use Messenger\Domain\Message\Entity\Message;
use Messenger\Infrastructure\Serializer\Fractal\TransformerInterface;

final class SerializeMessage extends TransformerAbstract implements TransformerInterface
{
    /**
     * @param Message $message
     * @return array
     */
    public function transform(Message $message) : array
    {
        return [
            'id'            => (string) $message->getId(),
            'email_address' => $message->hasEmail() ? (string) $message->getEmail() : null,
            'text'          => (string) $message->getText(),
            'created_at'    => $message->getCreatedAt()->format('Y-m-d H:i:s')
        ];
    }
}
