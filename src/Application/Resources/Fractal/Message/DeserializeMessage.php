<?php

namespace Messenger\Application\Resources\Fractal\Message;

use Ramsey\Uuid\Uuid;
use League\Fractal\TransformerAbstract;
use Messenger\Domain\Message\Entity\Message;
use Messenger\Domain\Message\ValueObject\Text;
use Messenger\Domain\Message\ValueObject\EmailAddress;
use Messenger\Infrastructure\Serializer\Fractal\TransformerInterface;

final class DeserializeMessage extends TransformerAbstract implements TransformerInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function transform(array $data) : array
    {
        return array_map(function (array $message) : Message {
            return $this->createMessageFrom($message);
        }, $data);
    }

    /**
     * @param array $message
     * @return Message
     */
    private function createMessageFrom(array $message) : Message
    {
        $id           = Uuid::fromString($message['id']);
        $text         = new Text($message['text']);
        $createdAt    = new \DateTime($message['created_at']);
        $emailAddress = null === $message['email_address'] ? null : new EmailAddress($message['email_address']);

        return new Message($id, $text, $emailAddress, $createdAt);
    }
}
