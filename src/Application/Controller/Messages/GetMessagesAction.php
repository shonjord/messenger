<?php declare(strict_types=1);

namespace Messenger\Application\Controller\Messages;

use Ramsey\Uuid\Uuid;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;
use Messenger\Application\Http\QueryParam;
use Psr\Http\Message\ServerRequestInterface;
use Messenger\Application\Controller\Common\ControllerInterface;
use Messenger\Domain\Message\Repository\MessageRepositoryInterface;
use Messenger\Infrastructure\Serializer\Common\SerializerInterface;

final class GetMessagesAction implements ControllerInterface
{
    private const MESSAGE_NOT_FOUND = ['Error' => 'Message not found'];

    /**
     * @var MessageRepositoryInterface
     */
    private $repository;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param MessageRepositoryInterface $repository
     * @param SerializerInterface $serializer
     */
    public function __construct(MessageRepositoryInterface $repository, SerializerInterface $serializer)
    {
        $this->repository = $repository;
        $this->serializer = $serializer;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws \Exception
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $query = (new QueryParam)($request->getQueryParams());

        if (isset($query->id)) {
            $message = $this->repository->findById(UUid::fromString($query->id));
            return new JsonResponse(
                null === $message ? self::MESSAGE_NOT_FOUND : $this->serializer->serialize($message)
            );
        }
        return new JsonResponse(
            $this->serializer->serialize($this->repository->findAll())
        );
    }
}
