<?php declare(strict_types=1);

namespace Messenger\Application\Controller\Common;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

interface ControllerInterface
{
    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return mixed
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface;
}
