<?php declare(strict_types=1);

namespace Messenger\Application\Controller\Root;

use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use Messenger\Application\Controller\Common\ControllerInterface;

final class GetRootAction implements ControllerInterface
{
    /**
     * @var array
     */
    private $content;

    /**
     * @param array $content
     */
    public function __construct(array $content)
    {
        $this->content = $content;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        return new JsonResponse($this->content);
    }
}
