<?php declare(strict_types=1);

namespace Messenger\Application\Controller\Root;

use Messenger\Application\Http\Body;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\Response\JsonResponse;
use Psr\Http\Message\ServerRequestInterface;
use Messenger\Domain\Message\Command\SaveMessageCommand;
use Messenger\Application\Controller\Common\ControllerInterface;
use Messenger\Infrastructure\CommandBus\Bus\CommandBusInterface;

final class PostMessageAction implements ControllerInterface
{
    /**
     * @var CommandBusInterface
     */
    private $commandBus;

    /**
     * @param CommandBusInterface $commandBus
     */
    public function __construct(CommandBusInterface $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    /**
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     */
    public function __invoke(ServerRequestInterface $request, ResponseInterface $response) : ResponseInterface
    {
        $body = (new Body)($request);

        try {
            $this->commandBus->handle(new SaveMessageCommand($body->email, $body->text));
        } catch (\Throwable $exception) {
            return new JsonResponse([
                'Message' => $exception->getMessage()
            ], 400);
        }

        return new JsonResponse([
            'Success' => 'Message was inserted successfully'
        ]);
    }
}
