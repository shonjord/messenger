<?php declare(strict_types=1);

namespace Messenger\Application\Exception;

abstract class AbstractValidationException extends \InvalidArgumentException
{

}
