<?php declare(strict_types=1);

namespace Messenger\Application\Container\Kernel;

use League\Container\Definition\DefinitionInterface;

interface ContainerInterface
{
    /**
     * @param $alias
     * @param null $concrete
     * @param bool $share
     * @return DefinitionInterface
     */
    public function add($alias, $concrete = null, $share = false);

    /**
     * @param $alias
     * @param array $args
     * @return mixed
     */
    public function get($alias, array $args = []);

    /**
     * @param $alias
     * @param null $concrete
     * @return DefinitionInterface
     */
    public function share($alias, $concrete = null);
}
