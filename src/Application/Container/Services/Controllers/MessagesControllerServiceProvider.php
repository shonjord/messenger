<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Controllers;

use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Application\Route\Routes\Messages\MessagesRoute;
use Messenger\Application\Controller\Messages\GetMessagesAction;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class MessagesControllerServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function addTo(ContainerInterface $container): void
    {
        $this->addMessagesControllerTo($container);
    }

    /**
     * @param ContainerInterface $container
     * @return void
     */
    private function addMessagesControllerTo(ContainerInterface $container) : void
    {
        $container->add(MessagesRoute::CONTROLLER, GetMessagesAction::class)
                  ->withArgument($container->get('repository.message'))
                  ->withArgument($container->get('fractal'));
    }
}
