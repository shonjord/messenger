<?php

namespace Messenger\Application\Container\Services\Controllers;

use Messenger\Application\Route\Routes\Root\RootRoute;
use Messenger\Application\Controller\Root\GetRootAction;
use Messenger\Application\Controller\Root\PostMessageAction;
use Messenger\Application\Route\Routes\Root\SaveMessageRoute;
use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class RootControllerServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     */
    public function addTo(ContainerInterface $container): void
    {
        $this->addIndexControllerTo($container);
        $this->addPostControllerTo($container);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addIndexControllerTo(ContainerInterface $container) : void
    {
        $container->add(RootRoute::CONTROLLER, GetRootAction::class)
                  ->withArgument(['Messenger' => ' Micro Service']);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addPostControllerTo(ContainerInterface $container) : void
    {
        $container->add(SaveMessageRoute::CONTROLLER, PostMessageAction::class)
                  ->withArgument($container->get('command.bus'));
    }
}
