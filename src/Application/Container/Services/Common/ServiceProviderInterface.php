<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Common;

use Messenger\Application\Container\Kernel\ContainerInterface;

interface ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function addTo(ContainerInterface $container) : void;
}
