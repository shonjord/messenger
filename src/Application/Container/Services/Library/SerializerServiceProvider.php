<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Library;

use Messenger\Infrastructure\Serializer\Jms\Serializer;
use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Infrastructure\Serializer\Fractal\FractalSerializer;
use Messenger\Application\Container\Services\Common\ServiceProviderInterface;

final class SerializerServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function addTo(ContainerInterface $container) : void
    {
        $this->addSerializerTo($container);
        $this->addFractalTo($container);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addSerializerTo(ContainerInterface $container) : void
    {
        $container->add('serializer', new Serializer);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addFractalTo(ContainerInterface $container) : void
    {
        $container->add('fractal', FractalSerializer::class);
    }
}
