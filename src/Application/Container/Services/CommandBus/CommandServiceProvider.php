<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\CommandBus;

use Messenger\Infrastructure\CommandBus\Bus\CommandBus;
use Messenger\Domain\Message\Handler\SaveMessageHandler;
use Messenger\Domain\Message\Command\SaveMessageCommand;
use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Infrastructure\CommandBus\Handler\HandlerInterface;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class CommandServiceProvider implements ServiceProviderInterface
{
    /**
     * @var array
     */
    private $handlers;

    public function __construct()
    {
        $this->handlers = [];
    }

    /**
     * @param ContainerInterface $container
     */
    public function addTo(ContainerInterface $container): void
    {
        $this->addSaveMessageHandlerTo($container);
        $this->addCommandBusTo($container);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addSaveMessageHandlerTo(ContainerInterface $container) : void
    {
        $handler = 'handler.save_message';

        $container->add($handler, SaveMessageHandler::class)->withArgument($container->get('repository.message'));
        $this->addHandler(SaveMessageCommand::class, $container->get($handler));
    }

    /**
     * @param ContainerInterface $container
     * @return void
     */
    private function addCommandBusTo(ContainerInterface $container) : void
    {
        $container->add('command.bus', new CommandBus($this->handlers));
    }

    /**
     * @param string $command
     * @param HandlerInterface $handler
     */
    private function addHandler(string $command, HandlerInterface $handler) : void
    {
        $this->handlers[$command] = $handler;
    }
}
