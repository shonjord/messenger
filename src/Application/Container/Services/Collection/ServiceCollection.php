<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Collection;

use Collections\Vector;
use Messenger\Application\Container\Services\Persistence\SqlServiceProvider;
use Messenger\Application\Container\Services\Persistence\RedisServiceProvider;
use Messenger\Application\Container\Services\Library\SerializerServiceProvider;
use Messenger\Application\Container\Services\CommandBus\CommandServiceProvider;
use Messenger\Application\Container\Services\Controllers\RootControllerServiceProvider;
use Messenger\Application\Container\Services\Repositories\MessageRepositoryServiceProvider;
use Messenger\Application\Container\Services\Controllers\MessagesControllerServiceProvider;

final class ServiceCollection extends Vector
{
    /**
     * @return ServiceCollection
     */
    public static function load() : ServiceCollection
    {
        return new self([
            new SerializerServiceProvider,
            new RedisServiceProvider,
            new SqlServiceProvider,
            new MessageRepositoryServiceProvider,
            new CommandServiceProvider,
            new RootControllerServiceProvider,
            new MessagesControllerServiceProvider
        ]);
    }
}
