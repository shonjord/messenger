<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Repositories;

use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Application\Container\services\common\ServiceProviderInterface;
use Messenger\Infrastructure\Persistence\Sql\Repository\Message\MessageRepository;

final class MessageRepositoryServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     */
    public function addTo(ContainerInterface $container) : void
    {
        $this->addRepositoryTo($container);
    }

    /**
     * @param ContainerInterface $container
     */
    private function addRepositoryTo(ContainerInterface $container) : void
    {
        $container->add('repository.message', MessageRepository::class)
                  ->withArgument($container->get('connection.sql'));
    }
}
