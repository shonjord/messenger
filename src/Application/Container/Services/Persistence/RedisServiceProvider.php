<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Persistence;

use Predis\Client;
use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Infrastructure\Persistence\Redis\Connection\Connection;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class RedisServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function addTo(ContainerInterface $container) : void
    {
        $this->addRedisTo($container);
    }

    /**
     * @param ContainerInterface $container
     * @return void
     */
    private function addRedisTo(ContainerInterface $container) : void
    {
        $container->add('connection.redis', Connection::class)
                  ->withArgument($this->getRedis());
    }

    /**
     * @return Client
     */
    private function getRedis() : Client
    {
        return new Client([
            'scheme' => getenv('REDIS_SCHEME'),
            'host'   => getenv('REDIS_HOST'),
            'port'   => getenv('REDIS_PORT'),
        ]);
    }
}
