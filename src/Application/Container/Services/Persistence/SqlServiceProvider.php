<?php declare(strict_types=1);

namespace Messenger\Application\Container\Services\Persistence;

use Messenger\Application\Container\Kernel\ContainerInterface;
use Messenger\Infrastructure\Persistence\Sql\Connection\Connection;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class SqlServiceProvider implements ServiceProviderInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function addTo(ContainerInterface $container) : void
    {
        $this->addPdoTo($container);
        $this->addConnectionTo($container);
    }

    /**
     * @param ContainerInterface $container
     * @return void
     */
    private function addPdoTo(ContainerInterface $container) : void
    {
        $container->add('pdo', $this->getPDO());
    }

    /**
     * @param ContainerInterface $container
     * @return void
     */
    private function addConnectionTo(ContainerInterface $container) : void
    {
        $container->add('connection.sql', Connection::class)
                  ->withArgument($container->get('pdo'))
                  ->withArgument($container->get('connection.redis'))
                  ->withArgument($container->get('fractal'));
    }

    /**
     * @return \PDO
     * @throws \PDOException
     */
    private function getPDO() : \PDO
    {
        try {
            return new \PDO(
                sprintf(
                    "%s:host=%s;port=%s;dbname=%s",
                    getenv('DB_DRIVER'),
                    getenv('DB_HOST'),
                    getenv('DB_PORT'),
                    getenv('DB_NAME')
                ),
                getenv('DB_USER'),
                getenv('DB_PASS'),
                [
                    \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
                    \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
                    \PDO::ATTR_EMULATE_PREPARES   => false,
                ]
            );
        } catch (\PDOException $PDOException) {
            throw $PDOException;
        }
    }
}
