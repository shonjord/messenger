<?php declare(strict_types=1);

namespace Messenger\Application\Kernel;

use Messenger\Application\Route\Kernel\Routing;
use Messenger\Application\Container\Kernel\Container;
use Messenger\Application\Container\Services\Collection\ServiceCollection;
use Messenger\Application\Container\services\common\ServiceProviderInterface;

final class Messenger
{
    /**
     * @var  Routing
     */
    private $route;

    /**
     * @var Container
     */
    private $container;

    /**
     * Messenger constructor.
     */
    public function __construct()
    {
        $this->container = new Container;
        $this->route     = new Routing;
    }

    /**
     * @return void
     * @throws \Exception
     */
    public function run() : void
    {
        $this->registerServices()
             ->registerRoutesToContainer();
    }

    /**
     * @return Messenger
     */
    private function registerServices() : Messenger
    {
        ServiceCollection::load()->each(function (ServiceProviderInterface $service) : void {
            $service->addTo($this->container);
        });

        return $this;
    }

    /**
     * @return Messenger
     */
    private function registerRoutesToContainer() : Messenger
    {
        $this->route->registerTo($this->container);

        return $this;
    }
}
