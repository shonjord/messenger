<?php declare(strict_types=1);

namespace Messenger\Application\Route\Routes\Messages;

use Messenger\Application\Http\HttpMethod\Method;
use Messenger\Application\Route\Routes\Parent\AbstractRoute;

final class MessagesRoute extends AbstractRoute
{
    protected const ALIAS   = 'messages';
    public const CONTROLLER = 'controller.messages';

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return Method::GET;
    }
}
