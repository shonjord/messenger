<?php declare(strict_types=1);

namespace Messenger\Application\Route\Routes\Parent;

use Messenger\Application\Route\Common\RouteInterface;

abstract class AbstractRoute implements RouteInterface
{
    protected const ALIAS   = '';
    public const CONTROLLER = '';

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return static::ALIAS;
    }

    /**
     * @return string
     */
    public function getController(): string
    {
        return static::CONTROLLER;
    }
}
