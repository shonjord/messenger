<?php declare(strict_types=1);

namespace Messenger\Application\Route\Routes\Root;

use Messenger\Application\Http\HttpMethod\Method;
use Messenger\Application\Route\Routes\Parent\AbstractRoute;

final class SaveMessageRoute extends AbstractRoute
{
    protected const ALIAS   = '/';
    public const CONTROLLER = 'controller.post_message';

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return Method::POST;
    }
}
