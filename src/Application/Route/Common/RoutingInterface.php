<?php declare(strict_types=1);

namespace Messenger\Application\Route\Common;

use Messenger\Application\Container\Kernel\ContainerInterface;

interface RoutingInterface
{
    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function registerTo(ContainerInterface $container) : void;
}
