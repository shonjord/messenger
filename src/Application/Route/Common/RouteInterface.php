<?php declare(strict_types=1);

namespace Messenger\Application\Route\Common;

interface RouteInterface
{
    /**
     * @return string
     */
    public function getMethod() : string;

    /**
     * @return string
     */
    public function getAlias() : string;

    /**
     * @return string
     */
    public function getController() : string;
}
