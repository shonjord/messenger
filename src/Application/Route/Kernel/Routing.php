<?php declare(strict_types=1);

namespace Messenger\Application\Route\Kernel;

use Zend\Diactoros\Response;
use Zend\Diactoros\ServerRequest;
use League\Route\RouteCollection;
use Psr\Http\Message\ResponseInterface;
use Zend\Diactoros\ServerRequestFactory;
use Zend\Diactoros\Response\SapiEmitter;
use Psr\Http\Message\ServerRequestInterface;
use Zend\Diactoros\Response\EmitterInterface;
use Messenger\Application\Route\Common\RouteInterface;
use Messenger\Application\Route\Common\RoutingInterface;
use Messenger\Application\Route\Collection\RoutingCollection;
use Messenger\Application\Container\Kernel\ContainerInterface;

final class Routing implements RoutingInterface
{
    private const RESPONSE = 'response';
    private const REQUEST  = 'request';
    private const EMITTER  = 'emitter';

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * @var RouteCollection
     */
    private $route;

    /**
     * @var ResponseInterface
     */
    private $response;

    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function registerTo(ContainerInterface $container): void
    {
        $this->setContainer($container)
             ->createRouteCollection()
             ->shareResponse()
             ->shareRequest()
             ->shareEmitter()
             ->mapRoutes()
             ->createResponse()
             ->emitResponse();
    }

    /**
     * @param ContainerInterface $container
     * @return Routing
     */
    private function setContainer(ContainerInterface $container) : Routing
    {
        $this->container = $container;

        return $this;
    }

    /**
     * @return Routing
     */
    private function createRouteCollection() : Routing
    {
        $this->route = new RouteCollection($this->container);

        return $this;
    }

    /**
     * @return Routing
     */
    private function shareResponse() : Routing
    {
        $this->share(self::RESPONSE, Response::class);

        return $this;
    }

    /**
     * @return Routing
     */
    private function shareRequest() : Routing
    {
        $this->share(self::REQUEST, function () : ServerRequest {
            return ServerRequestFactory::fromGlobals($_SERVER, $_GET, $_POST, $_COOKIE, $_FILES);
        });

        return $this;
    }

    /**
     * @return Routing
     */
    private function shareEmitter() : Routing
    {
        $this->share(self::EMITTER, SapiEmitter::class);

        return $this;
    }

    /**
     * @return Routing
     */
    private function mapRoutes() : Routing
    {
        RoutingCollection::load()->each(function (RouteInterface $route) : void {
            $this->route->map($route->getMethod(), $route->getAlias(), $this->get($route->getController()));
        });

        return $this;
    }

    /**
     * @return Routing
     */
    private function createResponse() : Routing
    {
        $this->response = $this->route->dispatch($this->getRequest(), $this->getResponse());

        return $this;
    }

    /**
     * @return void
     */
    private function emitResponse() : void
    {
        $this->getEmitter()->emit($this->response);
    }

    /**
     * @param string $alias
     * @param string|\Closure $concrete
     * @return Routing
     */
    private function share(string $alias, $concrete) : Routing
    {
        $this->container->share($alias, $concrete);

        return $this;
    }

    /**
     * @return ServerRequestInterface
     */
    private function getRequest() : ServerRequestInterface
    {
        return $this->get(self::REQUEST);
    }

    /**
     * @return ResponseInterface
     */
    private function getResponse() : ResponseInterface
    {
        return $this->get(self::RESPONSE);
    }

    /**
     * @return EmitterInterface
     */
    private function getEmitter() : EmitterInterface
    {
        return $this->get(self::EMITTER);
    }

    /**
     * @param string $alias
     * @return ServerRequestInterface|ResponseInterface|EmitterInterface
     */
    private function get(string $alias)
    {
        return $this->container->get($alias);
    }
}
