<?php declare(strict_types=1);

namespace Messenger\Application\Route\Collection;

use Collections\Vector;
use Messenger\Application\Route\Routes\Root\RootRoute;
use Messenger\Application\Route\Routes\Root\SaveMessageRoute;
use Messenger\Application\Route\Routes\Messages\MessagesRoute;

final class RoutingCollection extends Vector
{
    /**
     * @return RoutingCollection
     */
    public static function load() : RoutingCollection
    {
        return new self([
            new RootRoute,
            new SaveMessageRoute,
            new MessagesRoute
        ]);
    }
}
