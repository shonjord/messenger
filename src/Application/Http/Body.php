<?php declare(strict_types=1);

namespace Messenger\Application\Http;

use Psr\Http\Message\ServerRequestInterface;

final class Body
{
    /**
     * @param ServerRequestInterface $request
     * @return \stdClass
     */
    public function __invoke(ServerRequestInterface $request) : \stdClass
    {
        return json_decode((string) $request->getBody());
    }
}
