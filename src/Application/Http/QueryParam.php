<?php declare(strict_types=1);

namespace Messenger\Application\Http;

final class QueryParam
{
    /**
     * @param array|null $params
     * @return \stdClass
     */
    public function __invoke(?array $params) : \stdClass
    {
        return empty($params) ? new \stdClass : json_decode(json_encode($params));
    }
}
